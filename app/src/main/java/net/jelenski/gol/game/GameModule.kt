package net.jelenski.gol.game

import net.jelenski.gol.game.usecases.BigBoomUseCase
import net.jelenski.gol.game.usecases.ShuffleUseCase
import net.jelenski.gol.game.usecases.TimePassesUseCase
import net.jelenski.gol.game.view.GameActivity
import net.jelenski.gol.game.vm.GameViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val gameModule = module {
    scope(named<GameActivity>()) {
        viewModel { GameViewModel(bigBoomUseCase = get(), timePassesUseCase = get(), shuffleUseCase = get()) }
        scoped { BigBoomUseCase() }
        scoped { TimePassesUseCase() }
        scoped { ShuffleUseCase() }
    }
}